import boto3
from botocore.exceptions import ClientError
import config



# The character encoding for the email.
CHARSET = "UTF-8"

def sendEmail(my_subject,my_body):
    print("Welcome to sendEmail")
    print(my_subject)
    print(my_body)
    SUBJECT = my_subject
    BODY_TEXT = my_body


    # Create a new SES resource and specify a region.
    client = client = boto3.client(
        'ses',
        region_name=config.region_name,
        aws_access_key_id=config.aws_access_key_id,
        aws_secret_access_key=config.aws_secret_access_key
    )

    # Try to send the email.
    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    config.RECIPIENT,

                ],
            },
            Message={
                'Body': {
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source=config.SENDER,
            # If you are not using a configuration set, comment or delete the
            # following line
            # ConfigurationSetName=CONFIGURATION_SET,
        )
    # Display an error if something goes wrong.
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])
    return 