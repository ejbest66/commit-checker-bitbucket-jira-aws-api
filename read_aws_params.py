import boto3
import ast
import traceback
from botocore.exceptions import NoCredentialsError


def get_all_regions():
    ec2 = boto3.client('ec2')
    regions = [reg.get('RegionName') for reg in ec2.describe_regions().get('Regions')]
    return regions


def print_secret_values(region_name):
    secretsmanager_client = boto3.client('secretsmanager', region_name=region_name)
    all_screct = secretsmanager_client.list_secrets()

    if len(all_screct.get('SecretList')) > 0:

        print('Screts for region : ' + region_name)
        for secret in all_screct.get('SecretList'):
            secret_id = secret.get('ARN')
            secret_data = secretsmanager_client.get_secret_value(SecretId=secret_id)
            print("Scret name: {}. Screct Strings: \n".format(secret_data.get('Name')))
            key_val_data = ast.literal_eval(secret_data.get('SecretString'))
            for k, v in key_val_data.items():
                print("  Key: {} - Value: {}".format(k, v))
            print('-' * 50 + '\n')


def print_all_store_parameters():
    ssm_client = boto3.client('ssm')
    ssm_next_token = ' '
    while ssm_next_token is not None:
        ssm_descriptions = ssm_client.describe_parameters(MaxResults=1,
                                                          NextToken=ssm_next_token)
        store_params, ssm_next_token = ssm_descriptions.get('Parameters'), ssm_descriptions.get('NextToken')

        for param in store_params:
            param_detail = ssm_client.get_parameters(Names=[param.get('Name')], WithDecryption=True)
            print(
                'Parameter name: {} \n  - Data Type: {} \n  - and parameter value is: {}'.format(param.get('Name'),
                                                                                                 param_detail.get(
                                                                                                     'Parameters')[
                                                                                                     0].get(
                                                                                                     'DataType'),
                                                                                                 param_detail.get(
                                                                                                     'Parameters')[
                                                                                                     0].get('Value')
                                                                                                 ))
            print('-' * 50)


if __name__ == '__main__':
    try:
        # secret manager
        print('==' * 20 + "Start collect all secret manager" + "==" * 20)
        for region in get_all_regions():
            print_secret_values(region)
        print('==' * 20 + "End collect all secret manager" + "==" * 20 + "\n")

        # store_parameters
        print('==' * 20 + "Start collect all store parameters" + "==" * 20)
        print_all_store_parameters()
        print('==' * 20 + "End collect all store parameters" + "==" * 20 + "\n")
    except NoCredentialsError:
        print('You must login to AWS using `aws configure` command. You must install AWS CLI first')
        print(
            'Please check the reference here: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html')
    except Exception:
        print('Please see the error message:')
        print(traceback.format_exc())
