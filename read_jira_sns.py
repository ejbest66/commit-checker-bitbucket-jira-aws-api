#./ngrok http 5001

import sys 
sys.path.append("..")


from flask import Flask
from flask import request

import re
from typing import cast
from jira import JIRA, JIRAError
from jira.client import ResultList
from jira.resources import Issue

from bitbucket_webhooks import event_schemas
from bitbucket_webhooks import hooks
from bitbucket_webhooks import router
import config


from jira import JIRA

from ses import *
from sns import *

app = Flask(__name__)

jiraOptions = {'server': config.JIRA_SERVER}

my_email = config.JIRA_EMAIL
my_access_token = config.JIRA_TOKEN

# Try to send the email.

    

def fetch_ticket(my_comment, my_accountId, my_display_name):

    print("Comment=", my_comment)
    regex = r"([A-Z])\w+-([0-9])*"
    #test_str = "EDEL-1203This is a test"
    #matches = re.search(regex, test_str, re.MULTILINE)
    matches = re.search(regex, my_comment, re.MULTILINE)
    result =  matches.group(0) if matches else ""
    print(result)

    queryString =  "key = " + result 
    print("QueryString : " , queryString)
    print("Account_id= : " , my_accountId)
    print("Atlassian display name from BitBucket= : " , my_display_name)

    jira_assignee = ''
    jira_count = 0 
    jira_accountId = ''
    #jira_status = ''


    try:
        jira = JIRA(options=jiraOptions, basic_auth=( my_email, my_access_token))
        issues = cast(ResultList[Issue], jira.search_issues(queryString))
        for issue in issues:
            if issue.fields.assignee != None:
                print("issue.id=",  issue.id ,
                "issue.key=", issue.key, 
                #issue.fields.summary, 
                "Assignee=" , issue.fields.assignee, 
                "AssigneeEmail=" , issue.fields.assignee.emailAddress, 
                "Reporter=" , issue.fields.reporter, 
                "Created=" , issue.fields.created, 
                "Updated=",issue.fields.updated,
                "status=",issue.fields.status.name,
                "accountId",issue.fields.assignee.accountId)
                jira_assignee = issue.fields.assignee
                jira_accountId = issue.fields.assignee.accountId
                #jira_status = issue.fields.status.name

            else:
                print("Unassigned issue")
        jira_count = len( issues )
        print("The total ticket count=" , jira_count ) 

    except JIRAError as e:
        print(e.status_code, e.text)

    if jira_count != 1:
        if config.MODE == "ses":
            sendEmail("Invalid Jira ticket in commit","Failed to find the issue in Jira queryString="+ queryString )
        if config.MODE == "sns":
            publish("Invalid Jira ticket in commit","Failed to find the issue in Jira queryString="+ queryString )            
        return

    if ( jira_count == 1 )  and   str(jira_assignee).strip() !=   str(my_display_name).strip():
        if config.MODE == "ses":
            sendEmail("Assignee mismtach","Jira assignee= {} not matching BitBucket committer = {}".format(jira_assignee, my_display_name ))
        if config.MODE == "sns":
            publish("Assignee mismtach","Jira assignee= {} not matching BitBucket committer = {}".format(jira_assignee, my_display_name ))
        return

    if ( jira_count == 1 )  and   str(jira_assignee).strip() ==   str(my_display_name).strip():
        if config.MODE == "ses":
            sendEmail("Successful Commit","Jira assignee= {}  matched BitBucket committer = {}".format(jira_assignee, my_display_name ))
        if config.MODE == "sns":
            publish("Successful Commit","Jira assignee= {} matched BitBucket committer = {}".format(jira_assignee, my_display_name ))            
        return


    # if ( jira_count == 1 )  and   str(jira_assignee).strip() !=   str(my_display_name).strip():
    #     if config.MODE == "ses":
    #         sendEmail("Assignee mismtach","Jira assignee= {} not matching BitBucket committer = {}".format(jira_assignee, my_display_name ))
    #     if config.MODE == "sns":
    #         publish("Assignee mismtach","Jira assignee= {} not matching BitBucket committer = {}".format(jira_assignee, my_display_name ))
    #     return

    # if ( jira_count == 1 )  and   str(jira_assignee).strip() ==   str(my_display_name).strip():
    #     if config.MODE == "ses":
    #         sendEmail("Successful Commit","Jira assignee= {}  matched BitBucket committer = {}".format(jira_assignee, my_display_name ))
    #     if config.MODE == "sns":
    #         publish("Successful Commit","Jira assignee= {} matched BitBucket committer = {}".format(jira_assignee, my_display_name ))            
    #     return


    # #Uncomment only if needed    
    # if ( jira_count == 1 )  and   str(jira_accountId).strip() !=   str(my_accountId).strip():
    #     send_email("Atlassian Id from Jira ticket= {} not matching from BitBucket = {}".format(jira_accountId, my_accountId ))
    #     return
    # #Uncomment only if needed    
    # if ( jira_count == 1 )  and   str(jira_accountId).strip() ==   str(my_accountId).strip():
    #     send_email("Atlassian Id from Jira ticket= {} matching from BitBucket = {}".format(jira_accountId, my_accountId ))
    #     return


@app.route("/push_notify", methods=["GET", "POST"])
def tracking():
   if request.method == 'POST':
       data = request.get_json()
       print(data)

       comment = data['push']['changes'][0]['new']['target']['summary']['raw']
       account_id = data['push']['changes'][0]['new']['target']['author']['user']['account_id']
       display_name = data['push']['changes'][0]['new']['target']['author']['user']['display_name']
       fetch_ticket(comment, account_id, display_name) 

       return 'OK'


@hooks.repo_push
def _handle_repo_push(event: event_schemas.RepoPush):
    print(f"One or more commits pushed to: {event.repository.name}")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001)