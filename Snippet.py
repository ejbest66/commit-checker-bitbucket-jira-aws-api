import boto3
from botocore.exceptions import ClientError
import config


def publish(message,subject):

    topic_arn=config.topic_arn
    # Create a new SNS resource and specify a region.
    client = boto3.client(
        'sns',
        region_name=config.region_name,
        aws_access_key_id=config.aws_access_key_id,
        aws_secret_access_key=config.aws_secret_access_key
    )
    try:
        response = client.publish(
                TopicArn=topic_arn,
                Message=message,
                Subject=subject)

    # Display an error if something goes wrong.
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("SNS notification sent! Message ID:"),
        print(response['MessageId'])
    return 

publish(message="this is a message",subject="here is the test from the SNS Topic subject")
