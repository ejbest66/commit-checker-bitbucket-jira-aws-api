import boto3
from botocore.exceptions import ClientError
import config



def publish(my_subject,my_body):

    print("Welcome to publish")
    print("my_subject=",my_subject)
    print("my_body=",my_body)
    subject = my_subject
    message = my_body

    topic_arn=config.topic_arn
    # Create a new SNS resource and specify a region.
    client = boto3.client(
        'sns',
        region_name=config.region_name,
        aws_access_key_id=config.aws_access_key_id,
        aws_secret_access_key=config.aws_secret_access_key
    )
    try:
        response = client.publish(
                TopicArn=topic_arn,
                Message=message,
                Subject=subject)

    # Display an error if something goes wrong.
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("SNS notification sent! Message ID:"),
        print(response['MessageId'])
    return 

# We are calling from the main program.
# publish(message="message",subject="subject")